// wx on windows:
// https://wiki.wxwidgets.org/CodeBlocks_Setup_Guide
// https://forums.wxwidgets.org/viewtopic.php?t=42171
// https://forums.wxwidgets.org/viewtopic.php?t=38455
//deprecated stuff:
//https://wiki.wxwidgets.org/Updating_to_the_Latest_Version_of_wxWidgets

// wxWidgets "Hello world" Program
// For compilers that support precompilation, includes "wx/wx.h".
// Created 20190304

#include<iostream>

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include <wx/glcanvas.h>

#include "GlCanvas.hpp"

using namespace std;

enum { ID_Hello = 1 };

class MainFrame: public wxFrame {
  wxGLContext *m_context = nullptr;// needs wxGLCanvas
public:
  /* ********************************************************************************* */
  MainFrame(const wxString& title, const wxPoint& pos, const wxSize& size) : wxFrame(NULL, wxID_ANY, title, pos, size) {
    wxMenu *menuFile = new wxMenu;
    menuFile->Append(ID_Hello, "&Hello...\tCtrl-H", "Help string shown in status bar for this menu item");
    menuFile->AppendSeparator();
    menuFile->Append(wxID_EXIT);
    wxMenu *menuHelp = new wxMenu;
    menuHelp->Append(wxID_ABOUT);
    wxMenuBar *menuBar = new wxMenuBar;
    menuBar->Append( menuFile, "&File" );
    menuBar->Append( menuHelp, "&Help" );
    SetMenuBar( menuBar );
    CreateStatusBar();
    SetStatusText( "Welcome to wxWidgets!" );

    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnHello, this, ID_Hello);
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnAbout, this, wxID_ABOUT);
    Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnExit, this, wxID_EXIT);
    //Bind(wxEVT_COMMAND_MENU_SELECTED, &MainFrame::OnPaint, this, wxEVT_PAINT);
    Bind(wxEVT_PAINT, &MainFrame::OnPaint, this, wxID_ANY);
//  Bind(wxEVT_ERASE_BACKGROUND, &MainFrame::OnEraseBackground, this, -1);
//  Bind(wxEVT_ENTER_WINDOW, &MainFrame::OnMouse, this, -1);
//  Bind(wxEVT_LEAVE_WINDOW, &MainFrame::OnMouse, this, -1);

    //m_context = new wxGLContext(this);// https://wiki.wxwidgets.org/WxGLCanvas
    //Bind(wxEVT_KEY_DOWN, &MainFrame::KeyPressed, this, wxID_ANY);
    //Bind(wxEVT_CHAR_HOOK, &MainFrame::KeyPressed, this); // This works but blocks everything
    CreateUI();
  }

  ~MainFrame() {
    delete this->m_context;
  }

  void KeyPressed(wxKeyEvent& event) {
    wxChar ch = event.GetUnicodeKey();
    wxChar keycode = event.GetKeyCode();
    wxObject *evtobj = event.GetEventObject();
    cout << "Main KeyPressed:" << ch << ": evtobj:" << evtobj << ":\n";
    event.Skip();
  }

  void CreateUI() {
    // http://zetcode.com/gui/wxwidgets/layoutmanagement/

    int BorderWidth = 3;// or 10

    wxBoxSizer *VertSizer = new wxBoxSizer(wxVERTICAL);
    this->SetSizer(VertSizer);

    // wxPanel *TopPanel = new wxPanel(this, wxID_ANY);
    wxPanel *TopPanel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(250, 20));
    VertSizer->Add(TopPanel,
                   wxSTRETCH_NOT,// make vertically unstretchable
                   wxEXPAND |    // make horizontally stretchable
                   wxALL,        //   and make border all around
                   BorderWidth); // set border width
    TopPanel->SetBackgroundColour(wxColour(*wxCYAN));

    // Good diagrams: https://wiki.wxwidgets.org/WxSplitterWindow
    GlCanvas *ArtCanvas;
    if (false) {
      ArtCanvas = new GlCanvas(this);
      VertSizer->Add(ArtCanvas, wxEXPAND, wxEXPAND | wxALL, BorderWidth);
    } else {// horizontal belt panel
      wxPanel *BeltPanel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(250, 250));
      wxBoxSizer *HorizSizer = new wxBoxSizer(wxHORIZONTAL);
      BeltPanel->SetSizer(HorizSizer);
      VertSizer->Add(BeltPanel, wxEXPAND, wxEXPAND | wxALL, BorderWidth);
      BeltPanel->SetBackgroundColour(wxColour(*wxYELLOW));

      wxPanel *LeftPanel = new wxPanel(BeltPanel, wxID_ANY, wxDefaultPosition, wxSize(20, 250));
      HorizSizer->Add(LeftPanel, wxSTRETCH_NOT, wxEXPAND, BorderWidth);
      LeftPanel->SetBackgroundColour(wxColour(*wxRED));

      ArtCanvas = new GlCanvas(BeltPanel);
      HorizSizer->Add(ArtCanvas, wxEXPAND, wxEXPAND | wxALL, BorderWidth);

      wxPanel *RightPanel = new wxPanel(BeltPanel, wxID_ANY, wxDefaultPosition, wxSize(20, 250));
      HorizSizer->Add(RightPanel, wxSTRETCH_NOT, wxEXPAND, BorderWidth);
      RightPanel->SetBackgroundColour(wxColour(*wxGREEN));
    }

    wxPanel *BottomPanel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(250, 20));
    VertSizer->Add(BottomPanel,
                   wxSTRETCH_NOT,// make vertically unstretchable
                   wxEXPAND |    // make horizontally stretchable
                   wxALL,        //   and make border all around
                   BorderWidth); // set border width
    BottomPanel->SetBackgroundColour(wxColour(*wxBLUE));

    //SetSizerAndFit(VertSizer);
    VertSizer->SetSizeHints(this);
  }

private:
  /* ********************************************************************************* */
  void OnExit(wxCommandEvent& event) {
    Close( true );
  }
  void OnAbout(wxCommandEvent& event) {
    wxMessageBox( "This is a wxWidgets' Hello world sample", "About Hello World", wxOK | wxICON_INFORMATION );
  }
  void OnHello(wxCommandEvent& event) {
    wxLogMessage("Hello world from wxWidgets!");
  }
  void OnPaint(wxPaintEvent& event) {
    wxPaintDC dc(this);

    // draw a circle
    dc.SetBrush(*wxGREEN_BRUSH); // green filling
    dc.SetPen( wxPen( wxColor(255,0,0), 5 ) ); // 5-pixels-thick red outline
    dc.DrawCircle( wxPoint(200,100), 25 /* radius */ );

    // draw some text
    dc.DrawText(wxT("Testing"), 40, 60);

    wxCoord x1 = 50, y1 = 60;
    wxCoord x2 = 190, y2 = 60;

    dc.DrawLine(x1, y1, x2, y2);

    if (false) {
      wxPoint star[6];
      //border_points[0] = wxPoint( 0, 0 );
      star[0] = wxPoint(100, 60);
      star[1] = wxPoint(60, 150);
      star[2] = wxPoint(160, 100);
      star[3] = wxPoint(40, 100);
      star[4] = wxPoint(140, 150);
      dc.SetPen( *wxBLACK );// draw tab outline
      //dc.SetBrush(*wxTRANSPARENT_BRUSH);
      dc.DrawPolygon(WXSIZEOF(star), star);
    } else {
      dc.SetPen( *wxBLACK );// draw tab outline
      wxPointList StarPoints;
      StarPoints.Append(new wxPoint(100, 60));
      StarPoints.Append(new wxPoint(60, 150));
      StarPoints.Append(new wxPoint(160, 100));
      StarPoints.Append(new wxPoint(40, 100));
      StarPoints.Append(new wxPoint(140, 150));

#if FALSE
      for (int cnt=0; cnt<StarPoints.size(); cnt++) {
        wxNodeBase *pnt = StarPoints.Item(cnt);
        cout << "x:" << pnt->x << ", y:" << pnt->y << "\n";
      }
#endif

      dc.DrawPolygon(&StarPoints);
      dc.SetBrush(wxColor(255, 0, 0, 128));
      dc.DrawPolygon(&StarPoints, 20, 20);
//      gc->SetBrush(wxColor(255, 0, 0, 128));
//      gc->DrawPolygon(&StarPoints, 20, 20);
    }
    //delete gc;
  }
};

class MainApp: public wxApp {
public:
  virtual bool OnInit() {
    if (false) {
      wxFrame *frame = new wxFrame((wxFrame*) NULL, -1, _T("Hello wxWidgets World"));
      frame->CreateStatusBar();
      frame->SetStatusText(_T("Hello World"));
      frame->Show(true);
      SetTopWindow(frame);
      return true;
    } else {
      MainFrame *frame = new MainFrame( "Hello World", wxPoint(50, 50), wxSize(450, 340) );
      frame->Show( true );
      return true;
    }
  }
};

wxIMPLEMENT_APP(MainApp);

