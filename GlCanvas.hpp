#ifndef GlCanvas_hpp
#define GlCanvas_hpp

#include<iostream>
#include <vector>
#include <wx/wx.h>
#include <wx/glcanvas.h>

#if FALSE // FALSE // TRUE
#include "../Vectunes/Vectunes.hpp"
#endif // FALSE

#ifndef WIN32
#include <unistd.h> // FIXME: This work/necessary in Windows?
//Not necessary, but if it was, it needs to be replaced by process.h AND io.h
#endif

#if !wxUSE_GLCANVAS
#error "OpenGL required: set wxUSE_GLCANVAS to 1 and rebuild the library"
#endif

using namespace std;

/* ********************************************************************************* */
class GlCanvas: public wxGLCanvas { // Like DrawingPanel in Voices.java
public:
  //AudProject *MyProject = nullptr;
  //MainGui BigApp;// parent
  //Grabber Query; Grabber.DestinationGrabber DestQuery;
  int ScreenMouseX = 0, ScreenMouseY = 0;
  double MouseOffsetX = 0, MouseOffsetY = 0;
  //public IDrawable.IMoveable Floater = nullptr;// copy we are dragging around (in hover mode, no mouse buttons)
  //Point2D.Double results = new Point2D.Double();// used in multiple places as a return parameter for mapping

  // wxDefaultSize
  GlCanvas(wxWindow *parent) : wxGLCanvas(parent, wxID_ANY, NULL,  wxDefaultPosition, wxSize(250, 250), 0, wxT("GLCanvas"), wxNullPalette) {
    int argc = 1;
    char* argv[1] = { wxString((wxTheApp->argv)[0]).char_str() };

    Bind(wxEVT_PAINT, &GlCanvas::Paintit, this, wxID_ANY);
    Bind(wxEVT_MOTION, &GlCanvas::MouseMoved, this, wxID_ANY);
    Bind(wxEVT_LEFT_DOWN, &GlCanvas::MouseLeftDown, this, wxID_ANY);
    Bind(wxEVT_LEFT_UP, &GlCanvas::MouseLeftUp, this, wxID_ANY);
    Bind(wxEVT_RIGHT_DOWN, &GlCanvas::MouseRightDown, this, wxID_ANY);
    Bind(wxEVT_MOUSEWHEEL, &GlCanvas::MouseWheelMoved, this, wxID_ANY);
    Bind(wxEVT_MIDDLE_DOWN, &GlCanvas::MouseMiddleClick, this, wxID_ANY);
    Bind(wxEVT_ENTER_WINDOW, &GlCanvas::MouseEnterWindow, this, wxID_ANY);
    Bind(wxEVT_LEAVE_WINDOW, &GlCanvas::MouseLeaveWindow, this, wxID_ANY);
    Bind(wxEVT_KEY_DOWN, &GlCanvas::KeyPressed, this, wxID_ANY);
    //Bind(wxEVT_CHAR_HOOK, &GlCanvas::KeyPressed, this);
    Bind(wxEVT_KEY_UP, &GlCanvas::KeyReleased, this, wxID_ANY);
    Bind(wxEVT_SIZE, &GlCanvas::Resized, this, wxID_ANY);
    Init_Composition();
  }
  ~GlCanvas() {
    std::cout << "Destructor!" << "\n";
  }
  void Paintit(wxPaintEvent& WXUNUSED(event)) {
    Render();
  }
#ifdef Vectunes_hpp
  /* ********************************************************************************* */
  void FillVoice(Voice *voz, SoundFloat Duration) { // add voice bend points
    VoicePoint *vp0 = new VoicePoint();
    vp0->OctaveY = 5.0;
    vp0->TimeX = 0;
    voz->Add_Note(vp0);

    VoicePoint *vp1 = new VoicePoint();
    vp1->OctaveY = 8.0;
    vp1->TimeX = Duration * 0.531;
    voz->Add_Note(vp1);

    VoicePoint *vp2 = new VoicePoint();
    vp2->OctaveY = 2.0;
    vp2->TimeX = Duration;
    voz->Add_Note(vp2);

    voz->Recalc_Line_SubTime();
  }
  /* ********************************************************************************* */
  void FillVoice(Voice *voz) { // add voice bend points
    FillVoice(voz, 0.2);
  }
#endif // Vectunes_hpp
  /* ********************************************************************************* */
  void Init_Composition() {
#ifdef Vectunes_hpp
    Config conf;
    MetricsPacket metrics;
    metrics.MaxDuration = 0.0;
    metrics.MyProject = &conf;
    metrics.FreshnessTimeStamp = 2;

    if (true) {// Span - simple group with one delayed voice
      Voice *voz0;
      voz0 = new Voice();
      FillVoice(voz0);

      Voice::Voice_OffsetBox *vobox0 = voz0->Spawn_OffsetBox();
      vobox0->TimeX = 0.27;//0.11;

      GroupSong *gsong = new GroupSong();
      gsong->Add_SubSong(vobox0);

      gsong->Set_Project(&conf);
      gsong->Update_Guts(metrics);

      GroupSong::Group_OffsetBox *gobox = gsong->Spawn_OffsetBox();

      SingerBase *singer = gobox->Spawn_Singer();
      Chop_Test(singer, "Span");

      delete gsong;
    }
#endif // Vectunes_hpp
  }
  /* ********************************************************************************* */
  void PlotPolygon(std::vector<wxPoint> &points) {
    glBegin(GL_POLYGON);
    glColor3f(1.0, 1.0, 1.0);
    for (int cnt=0; cnt<points.size(); cnt++) {
      wxPoint *pnt = &(points.at(cnt));
      std::cout << "x:" << pnt->x << ", y:" << pnt->y << "\n";
      glVertex2f(pnt->x, pnt->y);
    }
    glEnd();
  }

  // events
  void FocusHack(wxMouseEvent &event) {
    if(event.Entering()) {
      printf("event.Entering\n");
      //SetFocus();
    }
    event.Skip();
  }
  void MouseMoved(wxMouseEvent &event) {
    printf("MouseMoved\n");
    //event.Skip();
  }
  void MouseLeftDown(wxMouseEvent &event) {
    printf("MouseLeftDown\n");
    SetFocus();
    //event.Skip();
  }
  void MouseLeftUp(wxMouseEvent &event) {
    printf("MouseLeftUp\n");
    //event.Skip();
  }
  void MouseRightDown(wxMouseEvent &event) {
    printf("MouseRightDown\n");
    SetFocus();
    //event.Skip();
  }
  void MouseWheelMoved(wxMouseEvent &mwevent) {
    printf("MouseWheelMoved\n");
    double XCtr, YCtr, Rescale;
    XCtr = mwevent.GetX();
    YCtr = mwevent.GetY();
    double finerotation = mwevent.GetWheelRotation();// getPreciseWheelRotation
    finerotation = -finerotation * 0.2;
    Rescale = std::pow(2.0, finerotation);//  Math.pow(2, finerotation);// range 0 to 1 to positive infinity
    /*
    GraphicBox.Graphic_OffsetBox gb = this.MyProject.GraphicRoot;
    gb.Zoom(XCtr, YCtr, Rescale);
    this.repaint();
    */
    //event.Skip();
  }
  /* ********************************************************************************* */
  void Zoom(double XCtr, double YCtr, double Scale) {// to do: put this in GraphicBox::Graphic_OffsetBox
    double XMov = XCtr - (Scale * XCtr);
    double YMov = YCtr - (Scale * YCtr);
    /*
        this.TimeX = XMov + (Scale * this.TimeX);
        this.OctaveY = YMov + (Scale * this.OctaveY);

        this.ScaleX *= Scale;
        this.ScaleY *= Scale;
    */
  }
  void MouseMiddleClick(wxMouseEvent &event) {
    printf("MouseMiddleClick\n");
    //event.Skip();
  }
  void MouseEnterWindow(wxMouseEvent &event) {
    printf("MouseEnterWindow\n");
    //SetFocus();
    //event.Skip();
  }
  void MouseLeaveWindow(wxMouseEvent &event) {
    printf("MouseLeaveWindow\n");
    //event.Skip();
  }
  void KeyPressed(wxKeyEvent& event) {
    if (event.GetModifiers() == wxMOD_CONTROL) {
    }
    if(event.GetKeyCode() == WXK_CONTROL_C && event.ControlDown()) {
      // WXK_CONTROL_A = 1,
    }
    if(event.GetKeyCode() == WXK_BACK && event.GetModifiers() == wxACCEL_CTRL) {
    }
    wxChar ch = event.GetUnicodeKey();
    wxChar keycode = event.GetKeyCode();
    wxObject *evtobj = event.GetEventObject();
    cout << "GlCanvas KeyPressed:" << ch << ": evtobj:" << evtobj << ":\n";
    event.Skip();
    return;
    if (event.GetKeyCode() == WXK_ESCAPE) {
      //SetCurrent(*m_glContext);
      //do your task here
      event.Skip(false); //not further process
      Refresh(false); //fire a paint-event which updates GL stuff
    }
  }
  void KeyReleased(wxKeyEvent& event) {
    printf("GlCanvas KeyReleased\n");
    event.Skip();
  }
  void Resized(wxSizeEvent& event) {
    printf("Resized\n");
    Refresh();
  }
private:
  /* ********************************************************************************* */
  void Render() {
    wxGLContext wxcon(this);
    wxGLCanvasBase::SetCurrent(wxcon);
    SetCurrent(wxcon);
    wxPaintDC(this);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT);

    double Left = -1.0, Right = 1.0;
    double Bottom = -1.0, Top = 1.0;
    double Near = -1.0, Far = 1.0;
    double Width = GetSize().x, Height = GetSize().y;
    double Factor = 0.25;

    glViewport(0, 0, Width, Height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(Left, Right, Bottom, Top, Near, Far);
    //glScaled(Factor, Factor, Factor);

    glBegin(GL_POLYGON);
    glColor3f(1.0, 1.0, 1.0);
    glVertex2f(-0.5, -0.5);// bottom left corner
    glVertex2f(-0.5, 0.5);// top left corner

    glVertex2f(0.0, 0.0);// concave vertex

    glVertex2f(0.5, 0.5);
    glVertex2f(0.5, -0.5);
    glColor3f(0.4, 0.5, 0.4);
    glVertex2f(0.0, -0.8);
    glEnd();

    glBegin(GL_POLYGON);
    //glColor3f(1.0, 0.0, 0.0);
    glColor4f(1.0, 0.0, 0.0, 0.75);
    glVertex2f(0.1, 0.1);
    glVertex2f(-0.1, 0.1);
    glVertex2f(-0.1, -0.1);
    glVertex2f(0.1, -0.1);
    glEnd();

    // using a little of glut
    glColor4f(0,0,1,1);
    //glutWireTeapot(0.4);

    glLoadIdentity();
    glColor4f(2,0,1,1);
    //glutWireTeapot(0.6);
    // done using glut

    glFlush();
    SwapBuffers();
  }
  /* ********************************************************************************* */
#if FALSE
  void Draw_Grid(DrawingContext &ParentDC) { // to do: put this in GraphicBox.
    double xloc, yloc;
    double MinX, MinY, MaxX, MaxY;// in audio coordinates
    int ScreenMinX, ScreenMinY, ScreenMaxX, ScreenMaxY;// in screen coordinates
    int X0, Y0;
    int width, height;

    MinX = Math::floor(ParentDC.ClipBounds.Min.x);
    MinY = Math::floor(ParentDC.ClipBounds.Min.y);
    MaxX = Math::ceil(ParentDC.ClipBounds.Max.x);
    MaxY = Math::ceil(ParentDC.ClipBounds.Max.y);

    ScreenMinX = (int) ParentDC.GlobalOffset->UnMapTime(MinX);
    ScreenMinY = (int) ParentDC.GlobalOffset->UnMapPitch(MinY);
    ScreenMaxX = (int) ParentDC.GlobalOffset->UnMapTime(MaxX);
    ScreenMaxY = (int) ParentDC.GlobalOffset->UnMapPitch(MaxY);

    if (ScreenMaxY < ScreenMinY) {// swap
      int temp = ScreenMaxY;
      ScreenMaxY = ScreenMinY;
      ScreenMinY = temp;
    }

    width = ScreenMaxX - ScreenMinX;
    height = ScreenMaxY - ScreenMinY;

    ParentDC.gr.setColor(Globals.ToAlpha(Color.lightGray, 100));// draw minor horizontal pitch lines
    for (double ysemi = MinY; ysemi < MaxY; ysemi += 1.0 / 12.0) {// semitone lines
      yloc = ParentDC.GlobalOffset.UnMapPitch(ysemi);
      ParentDC.gr.drawLine(ScreenMinX, (int) yloc, ScreenMaxX, (int) yloc);
    }

    ParentDC.gr.setColor(Globals.ToAlpha(Color.lightGray, 100));// draw minor vertical time lines
    for (double xsemi = MinX; xsemi < MaxX; xsemi += 1.0 / 4.0) {// 1/4 second time lines
      xloc = ParentDC.GlobalOffset.UnMapTime(xsemi);
      ParentDC.gr.drawLine((int) xloc, ScreenMinY, (int) xloc, ScreenMaxY);
    }

    Stroke PrevStroke = ParentDC.gr.getStroke();
    BasicStroke bs = new BasicStroke(2.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
    ParentDC.gr.setStroke(bs);
    ParentDC.gr.setColor(Globals.ToAlpha(Color.darkGray, 100));// draw major horizontal pitch lines
    for (double ycnt = MinY; ycnt < MaxY; ycnt++) {// octave lines
      yloc = ParentDC.GlobalOffset.UnMapPitch(ycnt);
      ParentDC.gr.drawLine(ScreenMinX, (int) yloc, ScreenMaxX, (int) yloc);
    }

    ParentDC.gr.setColor(Globals.ToAlpha(Color.darkGray, 100));// draw major vertical time lines
    for (double xcnt = MinX; xcnt < MaxX; xcnt++) {// 1 second time lines
      xloc = ParentDC.GlobalOffset.UnMapTime(xcnt);
      ParentDC.gr.drawLine((int) xloc, ScreenMinY, (int) xloc, ScreenMaxY);
    }

    // draw origin lines
    ParentDC.gr.setColor(Globals.ToAlpha(Color.red, 255));
    X0 = (int) ParentDC.GlobalOffset.UnMapTime(0);
    ParentDC.gr.drawLine(X0, ScreenMinY, X0, ScreenMaxY);

    Y0 = (int) ParentDC.GlobalOffset.UnMapPitch(0);
    ParentDC.gr.drawLine(ScreenMinX, Y0, ScreenMaxX, Y0);

    ParentDC.gr.setStroke(PrevStroke);
  }
#endif // FALSE
};

#endif // GlCanvas_hpp

#if FALSE
// junkyard
void Keys() {
  ConnectKeyDownEvent(this);
}
void ConnectKeyDownEvent(wxWindow* pclComponent) {
  if(pclComponent) { // https://wiki.wxwidgets.org/Catching_key_events_globally
    if (true) {
      pclComponent->Bind(wxEVT_KEY_DOWN, &GlCanvas::KeyPressed, this, wxID_ANY);
    } else {
      pclComponent->Connect(wxID_ANY,
                            wxEVT_KEY_DOWN,
                            wxKeyEventHandler(GlCanvas::KeyPressed),
                            (wxObject*) NULL,
                            this);
    }
    wxWindowListNode* pclNode = pclComponent->GetChildren().GetFirst();
    while(pclNode) {
      wxWindow* pclChild = pclNode->GetData();
      this->ConnectKeyDownEvent(pclChild);
      pclNode = pclNode->GetNext();
    }
  }
}

#endif // FALSE
